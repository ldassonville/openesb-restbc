package net.openesb.jbi.restbc.jbiadapter.inbound;



import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.UriBuilder;

import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.NetworkListener;
import org.glassfish.grizzly.http.server.ServerConfiguration;
import org.glassfish.grizzly.ssl.SSLContextConfigurator;
import org.glassfish.grizzly.threadpool.ThreadPoolConfig;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpContainer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ContainerFactory;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * InboundHttpListener.java
 *
 * @author Edward Chou
 */
public class InboundHttpListener {

    public static final String DEFAULT_LISTENER = "default-listener"; // NOI18N
    public static final String DEFAULT_LISTENER_SSL = "default-listener-ssl"; // NOI18N
    
    private String listenerName;
    private int port;
    private HttpServer webServer;
    
    private Map<String, HttpHandler> contextMap = new HashMap<String, HttpHandler> ();
    
    private static URI getBaseURI(int port) {
        return UriBuilder.fromUri("http://localhost/").port(port).build();
    }
    
    public InboundHttpListener(String listenerName, int port, int numThreads, SSLContextConfigurator sslConfig) {
        this.listenerName = listenerName;
        this.port = port;
        
        ThreadPoolConfig config = ThreadPoolConfig.defaultConfig().
                setCorePoolSize(numThreads).
                setMaxPoolSize(numThreads);
        webServer = GrizzlyHttpServerFactory.createHttpServer(getBaseURI(port));
        
   
        // assign the thread pool
        NetworkListener listener = webServer.getListeners().iterator().next();
        listener.getTransport().setWorkerThreadPoolConfig(config);
        
        
        ServerConfiguration configuration = webServer.getServerConfiguration();

        
        
       // this.webServer = new GrizzlyWebServer(port, ".", (sslConfig == null) ? false : true);
       // this.webServer.setCoreThreads(numThreads);
       // this.webServer.setMaxThreads(numThreads);
       //if (sslConfig != null) {
       //     webServer.setSSLConfig(sslConfig);
       //     ((SSLSelectorThread) webServer.getSelectorThread()).setNeedClientAuth(true);
       // }
    }
    
    public void start() throws Exception {
        webServer.start();
    }
    
    public void stop() throws Exception {
        webServer.shutdownNow();
    }
        
    public synchronized void registerContext(String context, ResourceConfig resourceConfig) throws Exception {
        if (contextMap.containsKey(context)) {
            throw new Exception("context already exists: " + context);
        }
        final ServerConfiguration config = webServer.getServerConfiguration();
        HttpHandler handler = ContainerFactory.createContainer(GrizzlyHttpContainer.class, resourceConfig);
        config.addHttpHandler(handler, context);
        
        contextMap.put(context, handler);
    }
    
    public synchronized void unregisterContext(String context) throws Exception {
    	HttpHandler removedValue = contextMap.remove(context);
        if (removedValue == null) {
            throw new Exception("context did not exist: " + context);
        }
        
        webServer.getServerConfiguration().removeHttpHandler(removedValue);
    }
        
    public Map<String, HttpHandler> getContextMap() {
        return Collections.unmodifiableMap(contextMap);
    }

    /**
     * @return the listenerName
     */
    public String getListenerName() {
        return listenerName;
    }

    /**
     * @return the port
     */
    public int getPort() {
        return port;
    }
    
}
